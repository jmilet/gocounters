package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"sync"
)

var wg sync.WaitGroup

const numberOfWorkers = 4

func main() {
	table := []string{"xaa", "xab", "xac", "xad", "xae", "xaf"}

	wg.Add(numberOfWorkers)
	for i := 0; i < numberOfWorkers; i++ {
		go func() {
			for {
				url := fmt.Sprintf("http://localhost:8000/%s", table[rand.Intn(6)])
				resp, err := http.Get(url)
				if err != nil {
					log.Fatal("Error in http get")
				}
				io.Copy(ioutil.Discard, resp.Body)
				resp.Body.Close()
			}
		}()
	}
	wg.Wait()
}
