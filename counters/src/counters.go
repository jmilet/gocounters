package main

import (
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"
)

const numberOfWorkers = 20

var wg sync.WaitGroup

type pair struct {
	key   string
	times int
}

func counter(workerNum int, input chan *pair, partials chan map[string]int) {
	cnt := make(map[string]int)

	for {
		select {
		case pair, ok := <-input:
			if !ok {
				break
			}
			//log.Printf("Worker: %d", workerNum)
			cnt[pair.key] += pair.times
		case <-time.After(time.Second * 3):
			if len(cnt) > 0 {
				partials <- cnt
				cnt = make(map[string]int)
			}
		}
	}
}

func accumulator(partials chan map[string]int, logger chan map[string]int) {
	totals := make(map[string]int)

	for {
		select {
		case partial, ok := <-partials:
			if !ok {
				break
			}
			mergeToDict(totals, partial)
		case <-time.After(time.Second * 3):
			logger <- totals
		}
	}
}

func dumper(logger chan map[string]int) {
	for counter := range logger {
		for k, v := range counter {
			fmt.Printf("%s -> %d\n", k, v)
		}
		fmt.Printf("------------------\n")
	}
}

func mergeToDict(dest map[string]int, origin map[string]int) {
	for k, v := range origin {
		dest[k] += v
	}
}

func getCounterName(url string) string {
	return strings.Split(url, "/")[1]
}

func main() {
	input := make(chan *pair)
	partials := make(chan map[string]int)
	logger := make(chan map[string]int)

	wg.Add(numberOfWorkers + 2)

	for workerNum := 0; workerNum < numberOfWorkers; workerNum++ {
		go counter(workerNum, input, partials)
	}

	go accumulator(partials, logger)
	go dumper(logger)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		input <- &pair{key: getCounterName(r.URL.Path), times: 1}
	})
	http.ListenAndServe(":8000", nil)
}
